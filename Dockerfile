FROM python:3.10

WORKDIR /nlp-group

RUN apt-get update && apt-get install -y \
    libhdf5-dev \
    && rm -rf /var/lib/apt/lists/*

COPY requirements-docker.txt /nlp-group/

RUN pip install --upgrade pip

RUN pip install -r requirements-docker.txt

COPY . /nlp-group/

RUN mkdir -p /nlp-group

RUN chmod -R 777 /nlp-group

EXPOSE 8000

CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]          


