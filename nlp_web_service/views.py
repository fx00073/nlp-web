from django.shortcuts import render, redirect
from django.conf import settings
from nlp_model.model_loader import predict
from django.contrib.auth.decorators import login_required
from nlp_model.models import Prediction
import csv
import os

def home(request):
    print(settings.TEMPLATES[0]['DIRS'])
    return render(request, 'home.html')


@login_required
def show_predict_form(request):
    return render(request, 'predict.html')

@login_required
def handle_predict(request):
    if request.method == 'POST':
        user_input = request.POST.get('user_input')
        if user_input:
            print(f"Received user input: {user_input}")
            prediction = predict(user_input)
            if prediction is not None:
                print(f"Prediction result: {prediction}")
                prediction_record = Prediction.objects.create(user=request.user, user_input=user_input, result=str(prediction))
                results_dir = "/nlp-group"
                os.makedirs(results_dir, exist_ok=True)
                results_path = os.path.join(results_dir, 'Predictions.csv')
                
                with open(results_path, 'a', newline='') as csvfile:
                    writer = csv.writer(csvfile)
                    if csvfile.tell() == 0:
                        writer.writerow([field.name for field in Prediction._meta.get_fields() if not field.is_relation])
                    writer.writerow([getattr(prediction_record, field.name) for field in Prediction._meta.get_fields() if not field.is_relation])
                return render(request, 'result.html', {'prediction': prediction_record.result})
            else:
                print("Prediction failed")
                return render(request, 'result.html', {'prediction': 'Prediction failed'})
        else:
            print("Error: Received empty user input.")
            return render(request, 'result.html', {'prediction': 'Error: Received empty user input.'})
    return redirect('predict_form')
