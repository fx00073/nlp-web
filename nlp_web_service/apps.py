from django.apps import AppConfig

class NlpWebServiceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nlp_web_service'
