import tensorflow as tf
from keras.models import load_model
import numpy as np
import json
from keras_preprocessing.sequence import pad_sequences
from keras.preprocessing.text import tokenizer_from_json

MAX_LEN = 165  
VOCAB_SIZE = 10000 

with open('nlp_model/word_tokenizer.json') as json_file:
    word_tokenizer_json = json.load(json_file)
    word_tokenizer = tokenizer_from_json(word_tokenizer_json)

with open('nlp_model/tag_tokenizer.json') as json_file:
    tag_tokenizer_json = json.load(json_file)
    tag_tokenizer = tokenizer_from_json(tag_tokenizer_json)

label_map = {
    0: 'O',        
    1: 'B-LF',    
    2: 'B-O',    
    3: 'I-LF',    
    4: 'B-AC',   
}

model_paths = ['nlp_model/LSTMbi_best.keras', 'nlp_model/LSTMbi_best.h5']

for model_path in model_paths:
    print(f"Attempting to load model from {model_path}")
    try:
        model = load_model(model_path)
        print("Model loaded successfully")
        break
    except Exception as e:
        print(f"Failed to load model from {model_path}: {e}")

else:
    print("Failed to load the model from any of the provided paths.")

def preprocess_input(text):
    sequences = word_tokenizer.texts_to_sequences([text])
    print(f"Tokenized sequences: {sequences}")
    if not sequences or not sequences[0]:
        print("Error: Tokenizer returned empty sequence. Please check the input text and tokenizer training.")
    padded_sequences = pad_sequences(sequences, maxlen=MAX_LEN, padding='post')
    print(f"Padded sequences: {padded_sequences}")
    return padded_sequences

def predict(text):
    processed_text = preprocess_input(text)
    try:
        prediction = model.predict(processed_text)
        predicted_class = np.argmax(prediction, axis=-1)
        print(f"Predicted class indices: {predicted_class}")

        predicted_labels = [label_map[i] for i in predicted_class[0]]
        print(f"Predicted labels: {predicted_labels}")
        return predicted_labels
    except Exception as e:
        print(f"Prediction failed: {e}")
        return None